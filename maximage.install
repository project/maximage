<?php
/**
 * @file
 * Maximage Module - Beta version - March 2014.
 */

/**
 * Install.
 */
function maximage_install() {
  $nf = FALSE;
  $path_to_css_dir = drupal_get_path('module', 'maximage') . '/css';
  $path_to_js_maximage = '';
  $path_to_js_cycle = '';
  node_types_rebuild();
  $types = node_type_get_types();
  node_add_body_field($types['maximage']);
  maximage_add_custom_fields();
  $path = libraries_get_path('maximage');
  if ($path != '') {
    $path_to_js_maximage = $path;
  }
  else {
    $nf = TRUE;
  }
  $path = libraries_get_path('cycle');
  if ($path != '') {
    $path_to_js_cycle = $path;
  }
  else {
    $nf = TRUE;
  }
  variable_set('jspathmaximage', $path_to_js_maximage);
  variable_set('jspathcycle', $path_to_js_cycle);
  variable_set('csspath', $path_to_css_dir);
  if ($nf) {
    $t = get_t();
    watchdog('Maximage', 'Please, configure the path of maximage and cycle.all js libraries.', array(), WATCHDOG_INFO);
    drupal_set_message($t('Maximage cannot find libraries. Please <a href="/admin/config/maximage">check the path</a> of jquery.maximage.js and jquery.cycle.js'), 'error');
  }
}
/**
 * Uninstall.
 */
function maximage_uninstall() {
  $maximage_type = 'maximage';
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, array(':type' => $maximage_type));
  $nodeids = array();
  foreach ($result as $row) {
    $nodeids[] = $row->nid;
  }
  node_delete_multiple($nodeids);
  maximage_delete_custom_fields();
  node_type_delete($maximage_type);
  field_purge_batch(500);
  db_drop_table('maximage');

  /* cleaning variables */
  variable_del('jspathmaximage');
  variable_del('jspathcycle');
  variable_del('csspath');
}

/**
 * Custom fields.
 */
function _maximage_custom_fields() {
  $t = get_t();
  return array(
    'maximage_image' => array(
      'field_name' => 'field_maximage',
      'label' => $t('Image'),
      'type' => 'image',
      'cardinality' => -1,
    ),
    'maximage_path' => array(
      'field_name' => 'field_maximagepath',
      'label' => $t('Path'),
      'type' => 'text_long',
    ),
    'maximage_jquery' => array(
      'field_name' => 'field_maximagejquery',
      'label' => $t('jQuery custom'),
      'type' => 'text_long',
    ),
    'maximage_html' => array(
      'field_name' => 'field_maximagehtml',
      'label' => $t('HTML custom'),
      'type' => 'text_long',
    ),
  );
}

/**
 * Custom field instances.
 */
function _maximage_custom_field_instances() {
  $t = get_t();
  return array(
    'maximage_image' => array(
      'field_name' => 'field_maximage',
      'type' => 'image',
      'label' => $t('Image'),
      'widget' => array(
        'type' => 'image',
      ),
      'required' => TRUE,
      'cardinality' => -1,
    ),
    'maximage_path' => array(
      'field_name' => 'field_maximagepath',
      'type' => 'text_long',
      'label' => $t('Path'),
      'widget' => array(
        'type' => 'textarea',
      ),
      'required' => TRUE,
      'description' => 'Insert here paths (one per line). * and &lt;front&gt; allowed.',
    ),
    'maximage_jquery' => array(
      'field_name' => 'field_maximagejquery',
      'type' => 'text_long',
      'label' => $t('jQuery custom'),
      'widget' => array(
        'type' => 'textarea',
      ),
      'description' => $t('Insert here your jQuery Maximage customization code'),
    ),
    'maximage_html' => array(
      'field_name' => 'field_maximagehtml',
      'type' => 'text_long',
      'label' => $t('HTML custom'),
      'widget' => array(
        'type' => 'textarea',
      ),
      'description' => $t('Insert here your jQuery HTML customization code'),
    ),
  );
}

/**
 * Add custom fields.
 */
function maximage_add_custom_fields() {
  foreach (_maximage_custom_fields() as $field) {
    field_create_field($field);
  }
  foreach (_maximage_custom_field_instances() as $fieldinstance) {
    $fieldinstance['entity_type'] = 'node';
    $fieldinstance['bundle'] = 'maximage';
    field_create_instance($fieldinstance);
  }
  $fieldinstance = field_info_instance('node', 'field_maximage', 'maximage');
  if (isset($fieldinstance['settings']['image_field_caption'])) {
    $fieldinstance['settings']['image_field_caption'] = TRUE;
    field_update_instance($fieldinstance);
  }
}

/**
 * Delete custom fields (needed by uninstall module).
 */
function maximage_delete_custom_fields() {
  foreach (array_keys(_maximage_custom_fields()) as $field) {
    field_delete_field($field);
  }
  $instances = field_info_instances('node', 'maximage');
  foreach ($instances as $fieldinstance) {
    field_delete_instance($fieldinstance);
  }
}

/**
 * Implements hook_enable().
 *
 * By default give maximage permissions to all roles.
 */
function maximage_enable() {
  $roles = user_roles(FALSE, NULL);
  $perms = array(
    'access maximage content',
  );
  foreach ($roles as $role) {
    $role_id = user_role_load_by_name($role)->rid;
    if (isset($role_id)) {
      user_role_grant_permissions($role_id, $perms);
    }
  }
}

/**
 * Impements hook_requirements().
 */
function maximage_requirements($phase) {
  $requirements = array();
  $t = get_t();
  if ($phase == 'install') {
    $path = libraries_get_path('cycle');
    if ($path == '') {
      $requirements['cycle'] = array(
        'title' => $t('Maximage warning'),
        'description' => $t('Cycle missing libraries, see /libraries/cycle folder'),
        'value' => '1.0',
        'severity' => REQUIREMENT_ERROR,
      );
    }
    $path = libraries_get_path('maximage');
    if ($path == '') {
      $requirements['maximage'] = array(
        'title' => $t('Maximage warning'),
        'description' => $t('Maximage missing libraries, see /libraries/maximage folder'),
        'value' => '1.0',
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }
  return $requirements;
}
